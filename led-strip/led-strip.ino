/*********
  
  Based on Rui Santos's tutorial https://RandomNerdTutorials.com/esp32-esp8266-web-server-outputs-momentary-switch/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files.
  
  The above copyright notice and this permission notice shall be included in all
  copies or substantial portions of the Software.
*********/

#ifdef ESP32
  #include <WiFi.h>
  #include <AsyncTCP.h>
#else
  #include <ESP8266WiFi.h>
  #include <ESPAsyncTCP.h>
#endif
#include <ESPAsyncWebServer.h>

// REPLACE WITH YOUR NETWORK CREDENTIALS
#define ssid	 YOURSSID
#define password YOURWIFIPASSWD

#include "html_page.h"

#define PIN_R 5
#define PIN_G 4
#define PIN_B 12
#define PIN_W 14

#define FADE_STEPS      64
#define FADE_TIME       2048
#define FADE_RESOLUTION 1024
#define FADE_STEP       FADE_RESOLUTION/FADE_STEPS
#define CHANGE_STEP     4

void notFound(AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "Not found");
}

uint32_t color_fade(uint32_t col_from, uint32_t col_to, uint16_t pos) {

    if(pos > FADE_RESOLUTION-FADE_STEP)
        return col_to;

    int r = ((col_to>>24) & 0xff) - ((col_from>>24) & 0xff);
    int g = ((col_to>>16) & 0xff) - ((col_from>>16) & 0xff);
    int b = ((col_to>>8 ) & 0xff) - ((col_from>>8 ) & 0xff);
    int w = ( col_to      & 0xff) - ( col_from      & 0xff);

    r = r * pos / FADE_RESOLUTION;
    g = g * pos / FADE_RESOLUTION;
    b = b * pos / FADE_RESOLUTION;
    w = w * pos / FADE_RESOLUTION;

    //Serial.printf("%08x -> %08x, r: %02x (%02x -> %02x), g: %02x (%02x -> %02x), b: %02x (%02x -> %02x), w: %02x (%02x -> %02x)\r\n", col_from, col_to, r, rf, rt, g, gf, gt, b, bf, bt, w, wf, wt);
    //Serial.printf("%08x -> %08x, ro: %02x, go: %02x, bo: %02x, wo: %02x\r\n", col_from, col_to, rf+r, gf+g, bf+b, wf+w);

    return col_from + r*(256*256*256) + g*(256*256) + b*(256) + w;

}

void set_color(uint32_t rgbw) {

    int r = ((rgbw >> 24) & 0xff) * 4;
    int g = ((rgbw >> 16) & 0xff) * 4;
    int b = ((rgbw >>  8) & 0xff) * 4;
    int w = ((rgbw >>  0) & 0xff) * 4;

    //Serial.printf("r: %d, g: %d, b: %d, w:%d\r\n", r, g, b, w);

    analogWrite(PIN_W, w);
    analogWrite(PIN_R, r);
    analogWrite(PIN_G, g);
    analogWrite(PIN_B, b);

}

uint32_t color_change(uint32_t rgbw, int cr, int cg, int cb, int cw) {

    int r = ((rgbw >> 24) & 0xff) + cr;
    int g = ((rgbw >> 16) & 0xff) + cg;
    int b = ((rgbw >>  8) & 0xff) + cb;
    int w = ((rgbw >>  0) & 0xff) + cw;

    if(r>255) r = 255;
    if(g>255) g = 255;
    if(b>255) b = 255;
    if(w>255) w = 255;

    if(r<0) r = 0;
    if(g<0) g = 0;
    if(b<0) b = 0;
    if(w<0) w = 0;

    return (r<<24 | g << 16 | b << 8 | w);

}


void set_color_fader(uint32_t col_from, uint32_t col_to, uint32_t fadet){

    Serial.printf("set_color_fader... 0x%08x -> 0x%08x\r\n", col_from, col_to);

    uint32_t f_color;
    if(fadet){
        for(int i=0; i<=FADE_STEPS; i++){
            f_color = color_fade(col_from, col_to, i*FADE_STEP);
            set_color(f_color);
            delay(fadet/FADE_STEPS);
        }
    } else {
        set_color(col_to);
    }
}


AsyncWebServer server(80);

bool mix = false;
bool off = false;
bool on = false;
bool change_color = false;

long int off_delay = 0;
long int mix_delay = 15;

uint32_t mix_mask = 0xffffffff;

uint32_t color = 0;
uint32_t old_color = 0;
uint32_t fade = FADE_TIME;

void setup() {
    Serial.begin(115200);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
        Serial.println("WiFi Failed!");
        return;
    }
    Serial.println();
    Serial.print("ESP IP Address: http://");
    Serial.println(WiFi.localIP());

    pinMode(PIN_R, OUTPUT);
    pinMode(PIN_G, OUTPUT);
    pinMode(PIN_B, OUTPUT);
    pinMode(PIN_W, OUTPUT);

    set_color(color);

    // Send web page to client
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
            request->send_P(200, "text/html", index_html);
    });

    // Receive an HTTP GET request
    server.on("/on", HTTP_GET, [] (AsyncWebServerRequest *request) {

            mix = false;
            on = true;

            if(request->hasParam("color")){
                old_color = color;
                color = strtoul( &(request->getParam("color")->value().c_str()[2]), NULL, 16);
            } else if(!color) {
                old_color = 0;
                color = 0x000000ff;
            }
           
            fade = FADE_TIME;
            change_color = true;

            request->send(200, "text/plain", "ok");
            });

    // Receive an HTTP GET request
    server.on("/off", HTTP_GET, [] (AsyncWebServerRequest *request) {
            off = true;
            if(request->hasParam("delay")){
                off_delay = request->getParam("delay")->value().toInt();
            }
            request->send(200, "text/plain", "ok");
            });

    server.on("/change", HTTP_GET, [] (AsyncWebServerRequest *request) {
            
            int cr = 0;
            int cg = 0;
            int cb = 0;
            int cw = 0;
            String val;

            mix = false;

            old_color = color;

            if(request->hasParam("r")){
                cr = CHANGE_STEP * request->getParam("r")->value().toInt();
            }
            if(request->hasParam("g")){
                cg = CHANGE_STEP * request->getParam("g")->value().toInt();
            }
            if(request->hasParam("b")){
                cb = CHANGE_STEP * request->getParam("b")->value().toInt();
            }
            if(request->hasParam("w")){
                cw = CHANGE_STEP * request->getParam("w")->value().toInt();
            }
            if(request->hasParam("a")){
                cr = CHANGE_STEP * request->getParam("a")->value().toInt();
                cg = CHANGE_STEP * request->getParam("a")->value().toInt();
                cb = CHANGE_STEP * request->getParam("a")->value().toInt();
                cw = CHANGE_STEP * request->getParam("a")->value().toInt();
            }

            color = color_change(color, cr, cg, cb, cw);

            fade = 0;
            change_color = true;

            request->send(200, "text/plain", "ok");
            });

    server.on("/mix", HTTP_GET, [] (AsyncWebServerRequest *request) {
            mix = true;
            on = true;
            if(request->hasParam("delay")){
                mix_delay = request->getParam("delay")->value().toInt();
            }
            if(request->hasParam("mask")){
                mix_mask = strtoul( &(request->getParam("mask")->value().c_str()[2]), NULL, 16);
            }
            request->send(200, "text/plain", "ok");
            });


    server.onNotFound(notFound);
    server.begin();
}

uint16_t mix_counter = 0;

void loop() {

    if(off && on) {
        if(off_delay)
            delay(1000*off_delay);
        set_color_fader(color, 0, FADE_TIME);
        off = false;
        on = false;
        mix = false;
        old_color = 0;
        off_delay = 0;
    }

    if(mix) {
        mix_counter = (mix_counter+1)%(mix_delay*10);
        if(!mix_counter){
            old_color = color;
            color = random(0xffffffff) & mix_mask;
            change_color = true;
            fade = FADE_TIME;
        }
    }

    if(change_color && on) {
        set_color_fader(old_color, color, fade);
        change_color = false;
    }

    delay(100);
}
