# RGBW LED strip driver with ESP8266

Driver for LED strips, controlled via web interface hosted on ESP8266 itself. Code written in Arduino. It uses non-blocking HTTP server, LEDs are controlled via SW PWM.

## Hardware

 * ESP8266 with at least 4 pins usable as GPIO output. I used 12E variant
 * 4 transistors capable to handle current for each color
 * 4 resistors cca 1 kohm (as pull-down connected to transistors base pin)
   * transistors and resistors are used from crappy RF operated LED strip driver from e-bay, which become with IR remote control -- so unusable.
 * 3 resistors cca 5 to 15 kohm
 * capacitors for input voltage filtering, 100uF, 100nF
 * voltage regulator with 3.3V as output and capable to hqandle 12V as input

### Wiring

...

## Software

...

### Commands

In form `http://driver/command?parameter=value`

If value is color, it must be 32bit (4byte) unsigned number in hexadecimal form. Each byte is for each color part: `0xRRGGBBWW`, for example: `0x9abcdef0`, so each color has 256 possible intensities.

 * ''on'' - turn on led strip. Use latest color before switch off or full white (0x000000FF)
   * parameters: `color` - value is color which you want
   * example:
      * `http://driver/on` - turn on latest color
      * `http://driver/on?color=0x7F7F0000` - turn on or change color to yellow
 * ''off'' - turn strip off
   * parameters: `delay` - wait `value` seconds before off, value is unsigned integer and means seconds before off.
 * ''mix'' - change randomly colors - first call starts changing colors (mix is stopped by all other calls).
   * parameters:
      * `delay` - how long wait before change, same like in `off`
      * `mask` - for masking bits of new random color, form same like `color` paramter. Applied `color & mask`
   * example:
     * `http://driver/mix` - turn on changing colors, default delay is 15s, default mask is `0xffffffff`
     * `http://driver/mix?delay=5&mask=0x007f7f00` - every 5 seconds change color to something between green and blue with half brightness
 * ''change'' - relatively to actual change color by specified number of steps. Each color can be changed in 64 steps from min to max (step mean change by 8 to color)
   * parameters:
     * `r`, `g`, `b`, `w` - change of each part of color
     * `a` - change ALL parts of color
   * example:
     * `http://driver/change?r=2&b=-5&w=-1` - change color from actual, for example 0x30FFab00 to 0x40d7ab00 (white - less then zero is zero)
     * `http://driver/change?a=-1` - change color from actual, for example 0x18283848 to 0x10203040


## License

Based on Rui Santos's tutorial https://RandomNerdTutorials.com/esp32-esp8266-web-server-outputs-momentary-switch/

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files.

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

